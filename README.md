#Curso SOA4ID

### Primera Tarea Corta "GastroTEC"

###### Estudiantes

* Luis Arturo Mora
* Sergio Varas


###### Requerimientos
* Android Studio 2.3.3
* Java 1.8.0_112_release-b06 amd64
* Min SDK 24 (Android 7.0 Nougat)

###### Instalación

Se debe clonar el repositorio git, cabe destacar, se debe tener un manejador de git. 
En Linux, generalmente se puede utilizar.
~~~~
git clone git@bitbucket.org:soa4id/gastrotec.git
~~~~
Es preferible tener instalado Android Studio, sin embargo el archivo APK de instalación se encuentra en el respositorio release.
