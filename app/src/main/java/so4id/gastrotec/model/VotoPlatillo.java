package so4id.gastrotec.model;

public class VotoPlatillo {

    private String mIdRestaurante;

    private String mIdUsuario;

    private boolean mEstadoVoto;

    public VotoPlatillo() {
    }


    public String getIdRestaurante() {
        return mIdRestaurante;
    }

    public void setIdRestaurante(String mIdRestaurante) {
        this.mIdRestaurante = mIdRestaurante;
    }

    public String getIdUsuario() {
        return mIdUsuario;
    }

    public void setIdUsuario(String mIdUsuario) {
        this.mIdUsuario = mIdUsuario;
    }


    public boolean isEstadoVoto() {
        return mEstadoVoto;
    }

    public void setEstadoVoto(boolean mEstadoVoto) {
        this.mEstadoVoto = mEstadoVoto;
    }
}
