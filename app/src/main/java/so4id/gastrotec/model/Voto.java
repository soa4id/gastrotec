package so4id.gastrotec.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Voto {

    public static String DB_ID_USUARIO = "idUsuario";
    public static String DB_ESTADO_VOTO = "estadoVoto";

    public static class VOTO {
        public static int NO_VOTO = 0;
        public static int LIKE = 1;
        public static int DISLIKE = 2;
    }

    private String mIdUsuario;
    private int mEstadoVoto;


    public Voto() {
    }

    public Voto(String pIdUsuario, int pEstadoVoto) {
        mIdUsuario = pIdUsuario;
        mEstadoVoto = pEstadoVoto;
    }
    public void setIdUsuario(String pIdUsuario) {
        this.mIdUsuario = pIdUsuario;
    }

    public String getIdUsuario() {
        return mIdUsuario;
    }

    public int getEstadoVoto() {
        return mEstadoVoto;
    }

    public void setEstadoVoto(int mEstadoVoto) {
        this.mEstadoVoto = mEstadoVoto;
    }
}
