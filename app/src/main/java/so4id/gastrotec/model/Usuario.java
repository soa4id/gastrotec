package so4id.gastrotec.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by sergio on 20/09/17.
 */

@IgnoreExtraProperties
public class Usuario {

    public static final String USR_LABEL = "estudiantes";
    public static final String USR_CARNET = "carnet";
    public static final String USR_CARRERA = "carrera";
    public static final String USR_ID = "idUsuario";
    public static final String USR_TYPE = "tipo";
    public static final String USR_ADMIN = "admin";
    public static final String USR_NML = "normal";

    public String carnet;
    public String carrera;
    public String tipo;

    @Exclude
    public String id;

    public Usuario() {

    }

    public Usuario(Builder builder) {
        this.id = builder.id;
        this.carnet = builder.carnet;
        this.carrera = builder.carrera;
        this.tipo = builder.tipo;
    }

    @Exclude
    public String getId() {
        return id;
    }

    public String getCarnet() {
        return carnet;
    }

    public String getCarrera() {
        return carrera;
    }

    public String getTipo() {
        return tipo;
    }

    public static class Builder {
        private final String id;
        private final String carnet;
        private final String carrera;
        private final String tipo;

        public Builder(String id, String carnet, String carrera, String tipo) {
            this.id = id;
            this.carnet = carnet;
            this.carrera = carrera;
            this.tipo = tipo;
        }

    }

}
