package so4id.gastrotec.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by sergio on 21/09/17.
 */

@IgnoreExtraProperties
public class Horario {

    public static final String HOR_ID = "horId";
    public static final String HOR_LABEL = "horarios";
    public static final String HOR_REST_ID = "idRestaurante";
    public static final String HOR_DATE = "fecha";

    @Exclude
    public String idHorario;
    public String idPlatillo;
    public String idRestaurante;
    public String fecha;
    public float duracion;

    public Horario() {
    }

    public Horario(String idPlatillo, String idRestaurante, String fecha, float duracion) {
        this.idPlatillo = idPlatillo;
        this.idRestaurante = idRestaurante;
        this.fecha = fecha;
        this.duracion = duracion;
    }

    //    public String getFechaHumanReadable() {
//        java.util.Date time = new java.util.Date((long) fecha * 1000);
//        String fechaStr = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(time);
//        return fechaStr;
//    }

    @Exclude
    public String getIdHorario() {
        return idHorario;
    }

    @Exclude
    public void setIdHorario(String idHorario) {
        this.idHorario = idHorario;
    }

    public String getIdPlatillo() {
        return idPlatillo;
    }

    public void setIdPlatillo(String idPlatillo) {
        this.idPlatillo = idPlatillo;
    }

    public String getIdRestaurante() {
        return idRestaurante;
    }

    public void setIdRestaurante(String idRestaurante) {
        this.idRestaurante = idRestaurante;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getDuracion() {
        return duracion;
    }

    public void setDuracion(float duracion) {
        this.duracion = duracion;
    }


//    public String toString() {
//        return this.getFechaHumanReadable() + " " + this.duracion + "h";
//    }
}
