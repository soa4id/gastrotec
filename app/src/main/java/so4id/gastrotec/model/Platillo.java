package so4id.gastrotec.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Platillo {

    public static final String VAR_NOMBRE = "nombre";
    public static final String VAR_BALANCE_VOTOS = "balanceVotos";
    public static final String PLAT_LABEL = "platillos";

    @Exclude
    public String mIdRestaurante;
    @Exclude
    public String mIdPlatillo;

    private String mNombre;
    private int mBalanceVotos;

    public Platillo() {

    }

    public Platillo(String pIdRestaurante, String pNombre, int pBalanceVotos) {
        mIdRestaurante = pIdRestaurante;
        mNombre = pNombre;
        mBalanceVotos = pBalanceVotos;
    }

    @Exclude
    public String getIdRestaurante() {
        return mIdRestaurante;
    }

    @Exclude
    public void setIdRestaurante(String mIdRestaurante) {
        this.mIdRestaurante = mIdRestaurante;
    }

    @Exclude
    public String getIdPlatillo() {
        return mIdPlatillo;
    }

    @Exclude
    public void setIdPlatillo(String idPlatillo) {
        this.mIdPlatillo = idPlatillo;
    }

    public String getNombre() {
        return mNombre;
    }

    public void setNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public int getBalanceVotos() {
        return mBalanceVotos;
    }

    public void setBalanceVotos(int mBalanceVotos) {
        this.mBalanceVotos = mBalanceVotos;
    }

    public String toString() {
        return mNombre;
    }

}
