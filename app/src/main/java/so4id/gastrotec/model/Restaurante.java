package so4id.gastrotec.model;

public class Restaurante {

    public static final String REST_ID = "restId";
    public static final String REST_LABEL = "restaurantes";
    public static final String REST_VOTES = "balanceVotos";

    public String nombre;
    public String direccion;
    public String horarioServicio;
    public int balanceVotos;

    public Restaurante() {
    }

    public Restaurante(String nombre, String direccion, String horarioServicio, int balanceVotos) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.horarioServicio = horarioServicio;
        this.balanceVotos = balanceVotos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getHorarioServicio() {
        return horarioServicio;
    }

    public void setHorarioServicio(String horarioServicio) {
        this.horarioServicio = horarioServicio;
    }

    public int getBalanceVotos() {
        return balanceVotos;
    }

    public void setBalanceVotos(int balanceVotos) {
        this.balanceVotos = balanceVotos;
    }
}