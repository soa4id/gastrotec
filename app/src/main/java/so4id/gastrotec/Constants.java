package so4id.gastrotec;

/**
 * Created by amora on 11/2/17.
 */

public class Constants {
    public static final String FIREBASE_DATABASE_CHILD_RESTAURANTS = "restaurantes";
    public static final String FIREBASE_DATABASE_CHILD_DISHES = "platillos";
    public static final String FIREBASE_DATABASE_CHILD_VOTES = "votos";
    public static final String FIREBASE_DATABASE_CHILD_TIME= "horarios";

    public static final String DATABASE_CAREER = "carreras";
}
