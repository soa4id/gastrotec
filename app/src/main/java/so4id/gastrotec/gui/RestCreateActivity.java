package so4id.gastrotec.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Restaurante;
import so4id.gastrotec.model.Usuario;

public class RestCreateActivity extends AppCompatActivity {

    @BindView(R.id.rest_name_create)
    EditText mNameEditText;
    @BindView(R.id.rest_location_create)
    EditText mLocationEditText;
    @BindView(R.id.working_hours_create)
    EditText mWorkingHoursEditText;
    private int mType;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_create);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mType = extras.getInt(Usuario.USR_TYPE);
        }
        mDatabase = FirebaseDatabase.getInstance().getReference();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Usuario.USR_TYPE, mType);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mType = savedInstanceState.getInt(Usuario.USR_TYPE);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @OnClick(R.id.create_restaurant_button)
    void onClick(View v) {
        String nombre = mNameEditText.getText().toString();
        String direccion = mLocationEditText.getText().toString();
        String horario = mWorkingHoursEditText.getText().toString();
        if (TextUtils.isEmpty(nombre)) {
            Toast.makeText(getApplicationContext(), R.string.rest_no_name_provided,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(direccion)) {
            Toast.makeText(getApplicationContext(), R.string.rest_no_address_provided,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(horario)) {
            Toast.makeText(getApplicationContext(), R.string.rest_no_working_hours_provided,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Restaurante restaurante = new Restaurante(nombre, direccion, horario, 0);
        DatabaseReference databaseReference = mDatabase.child(Restaurante.REST_LABEL).push();
        final String restId = databaseReference.getKey();
        databaseReference.setValue(restaurante)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), R.string.rest_create_failed,
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.rest_created_successfully,
                                    Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), RestProfileActivity.class);
                            intent.putExtra(Restaurante.REST_ID, restId);
                            intent.putExtra(Usuario.USR_TYPE, mType);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
    }
}
