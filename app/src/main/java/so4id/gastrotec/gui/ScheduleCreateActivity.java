package so4id.gastrotec.gui;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Horario;
import so4id.gastrotec.model.Platillo;
import so4id.gastrotec.model.Restaurante;

public class ScheduleCreateActivity extends AppCompatActivity {

    @BindView(R.id.sched_date)
    Button mDateButton;

    @BindView(R.id.sched_time)
    Button mTimeButton;

    @BindView(R.id.sched_duration)
    Button mDurationButton;

    @BindView(R.id.dishes_spinner)
    Spinner mDishesSpinner;

    private DatePickerDialog mDatePicker;
    private TimePickerDialog mTimePicker;
    private TimePickerDialog mDurationPicker;

    private String mFecha;
    private String mHora;
    private float mDuracion = 0;

    private Calendar mCalendar;
    private String mRestauranteId;
    private String mPlatilloId;
    private DatabaseReference mDatabase;
    private List<Platillo> mDishes;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Restaurante.REST_ID, mRestauranteId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mRestauranteId = savedInstanceState.getString(Restaurante.REST_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_create);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mRestauranteId = extras.getString(Restaurante.REST_ID);
        }
        mDishes = new ArrayList<Platillo>();
        mCalendar = Calendar.getInstance();
        int cDay = mCalendar.get(Calendar.DAY_OF_MONTH);
        int cMonth = mCalendar.get(Calendar.MONTH);
        int cYear = mCalendar.get(Calendar.YEAR);
        int cHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        int cMinute = mCalendar.get(Calendar.MINUTE);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Restaurante.REST_LABEL).child(mRestauranteId)
                .child(Platillo.PLAT_LABEL).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot dishSnapshot : dataSnapshot.getChildren()) {
                    Platillo platillo = dishSnapshot.getValue(Platillo.class);
                    platillo.setIdPlatillo(dishSnapshot.getKey());
                    mDishes.add(platillo);
                }
                ArrayAdapter<Platillo> dishesAdapter = new ArrayAdapter<Platillo>(getApplicationContext(),
                        android.R.layout.simple_spinner_item, mDishes);
                dishesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mDishesSpinner.setAdapter(dishesAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        mDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker dp, int year, int month, int day) {
                mCalendar.set(year, month, day);
                SimpleDateFormat format = new SimpleDateFormat(getResources().getString(R.string.date_pattern));
                mFecha = format.format(mCalendar.getTime());
                mDateButton.setText(mFecha);
            }
        }, cYear, cMonth, cDay);

        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker tp, int hour, int minute) {
                mHora = ((hour < 10) ? "0" + hour : hour) + ":"
                        + ((minute < 10) ? "0" + minute : minute);
                mTimeButton.setText(mHora);
            }
        }, cHour, cMinute, true);

        mDurationPicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker tp, int hour, int minute) {
                String duracionStr = ((hour < 10) ? "0" + hour : hour) + ":"
                        + ((minute < 10) ? "0" + minute : minute);
                mDurationButton.setText(duracionStr + "h");
                mDuracion = hour + (minute / 60);
            }
        }, 0, 0, true);
    }

    @OnItemSelected(R.id.dishes_spinner)
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        mPlatilloId = mDishes.get(pos).getIdPlatillo();
    }

    @OnClick({R.id.sched_create,
            R.id.sched_date,
            R.id.sched_time,
            R.id.sched_duration})
    void onClick(View v) {

        switch (v.getId()) {

            case R.id.sched_create:
                schedCreate();
                break;
            case R.id.sched_date:
                mDatePicker.show();
                break;
            case R.id.sched_time:
                mTimePicker.show();
                break;
            case R.id.sched_duration:
                mDurationPicker.show();
                break;
        }
    }

    private void schedCreate() {
        if (TextUtils.isEmpty(mFecha)) {
            Toast.makeText(getApplicationContext(), R.string.schedule_no_date_provided,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(mHora)) {
            Toast.makeText(getApplicationContext(), R.string.schedule_no_time_provided,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (mDuracion == 0) {
            Toast.makeText(getApplicationContext(), R.string.schedule_no_duration_provided,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (mPlatilloId.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.schedule_no_dish_provided,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        mFecha = mFecha + " " + mHora;
        Horario horario = new Horario(mPlatilloId, mRestauranteId, mFecha, mDuracion);
        mDatabase.child(Horario.HOR_LABEL).push().setValue(horario)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), R.string.schedule_create_failed,
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.schedule_created_successfully,
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
