package so4id.gastrotec.gui;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Horario;

public class ScheduleAdapter extends FirebaseRecyclerAdapter<Horario, ScheduleAdapter.Viewholder> {

    public ScheduleAdapter(FirebaseRecyclerOptions<Horario> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(Viewholder holder, int position, Horario model) {
        final Horario horario = this.getItem(position);
        holder.mTitleHorary.setText(horario.getFecha());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DishListActivity.class);
                intent.putExtra(Horario.HOR_DATE, horario.getFecha());
                intent.putExtra(Horario.HOR_REST_ID, horario.getIdRestaurante());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schedule_row, parent, false);
        return new ScheduleAdapter.Viewholder(view);
    }

    static class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.horary_title)
        TextView mTitleHorary;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
