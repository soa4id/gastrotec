package so4id.gastrotec.gui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Horario;
import so4id.gastrotec.model.Platillo;
import so4id.gastrotec.model.Restaurante;
import so4id.gastrotec.model.Usuario;
import so4id.gastrotec.model.VotoPlatillo;


public class DishListActivity extends AppCompatActivity  {

    @BindView(R.id.dishes_recycler)
    RecyclerView mDishList;
    @BindView(R.id.sched_date)
    Button mSchedDateButton;
    private String mIdRestaurante;
    private String mIdUsuario;
    private String mIdHorario;
    private String mFechaStr;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Usuario.USR_ID, mIdUsuario);
        outState.putString(Restaurante.REST_ID, mIdRestaurante);
        outState.putString(Horario.HOR_ID, mIdHorario);
        outState.putString(Horario.HOR_DATE, mFechaStr);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mIdUsuario = savedInstanceState.getString(Usuario.USR_ID);
        mIdRestaurante = savedInstanceState.getString(Restaurante.REST_ID);
        mIdHorario = savedInstanceState.getString(Horario.HOR_ID);
        mFechaStr = savedInstanceState.getString(Horario.HOR_DATE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_list);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mFechaStr = extras.getString(Horario.HOR_DATE);
            mIdRestaurante = extras.getString(Restaurante.REST_ID);
            mIdUsuario = extras.getString(Usuario.USR_ID);
            mIdHorario = extras.getString(Horario.HOR_ID);
        }
        mSchedDateButton.setText(mFechaStr);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
