package so4id.gastrotec.gui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import so4id.gastrotec.R;


public class RestMenuFragment extends Fragment {

    public RestMenuFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_rest_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

}
