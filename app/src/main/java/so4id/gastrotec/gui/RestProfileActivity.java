package so4id.gastrotec.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import so4id.gastrotec.Constants;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Horario;
import so4id.gastrotec.model.Platillo;
import so4id.gastrotec.model.Restaurante;
import so4id.gastrotec.model.Usuario;

public class RestProfileActivity extends AppCompatActivity {

    @BindView(R.id.rest_profile_btn_name)
    Button mRestNameButton;
    @BindView(R.id.rest_profile_btn_location)
    Button mLocationButton;
    @BindView(R.id.rest_profile_btn_attention_time)
    Button mAttentionButton;
    @BindView(R.id.liked_dish_button)
    Button mLikedDishButton;
    @BindView(R.id.disliked_dish_button)
    Button mDislikedDishButton;
    @BindView(R.id.dishes_text)
    TextView mDishesTextButton;
    @BindView(R.id.liked_dish_layout)
    LinearLayout mLikedDishLayout;
    @BindView(R.id.disliked_dish_layout)
    LinearLayout mDislikedDishLayout;
    @BindView(R.id.liked_dish_view)
    View mLikedDishView;
    @BindView(R.id.dishes_recycler)
    RecyclerView mScheduleList;
    ScheduleAdapter mScheduleAdapter;
    String mRestauranteId;
    String mUserUID;
    int mType = 0;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Restaurante.REST_ID, mRestauranteId);
        outState.putString(Usuario.USR_ID, mUserUID);
        outState.putInt(Usuario.USR_TYPE, mType);
        super.onSaveInstanceState(outState);
    }

    void initFirebase() {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference ref1 = db
                .getReference(Constants.FIREBASE_DATABASE_CHILD_RESTAURANTS)
                .child(mRestauranteId);
        ref1.keepSynced(true);
        ref1.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Restaurante restaurante = dataSnapshot.getValue(Restaurante.class);
                mRestNameButton.setText(restaurante.getNombre());
                mLocationButton.setText(restaurante.getDireccion());
                mAttentionButton.setText(restaurante.getHorarioServicio());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        Query baseQuery = db
                .getReference(Restaurante.REST_LABEL)
                .child(mRestauranteId)
                .child(Platillo.PLAT_LABEL)
                .orderByChild(Platillo.VAR_BALANCE_VOTOS);
        baseQuery.keepSynced(true);
        Query likedQuery = baseQuery.limitToLast(1);
        likedQuery.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Platillo platillo = dataSnapshot.getValue(Platillo.class);
                mLikedDishButton.setText(platillo.getNombre());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Platillo platillo = dataSnapshot.getValue(Platillo.class);
                mLikedDishButton.setText(platillo.getNombre());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        Query dislikeQuery = baseQuery.limitToFirst(1);
        dislikeQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Platillo platillo = dataSnapshot.getValue(Platillo.class);
                mDislikedDishButton.setText(platillo.getNombre());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Platillo platillo = dataSnapshot.getValue(Platillo.class);
                mDislikedDishButton.setText(platillo.getNombre());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mScheduleList.setLayoutManager(layoutManager);
        DatabaseReference  ref = FirebaseDatabase.getInstance()
                .getReference(Horario.HOR_LABEL);
        Query query = ref
                .orderByChild(Horario.HOR_REST_ID).equalTo(mRestauranteId);
        query.keepSynced(true);
        FirebaseRecyclerOptions<Horario> options =
                new FirebaseRecyclerOptions.Builder<Horario>()
                        .setQuery(query, Horario.class)
                        .build();

        mScheduleAdapter = new ScheduleAdapter(options);
        mScheduleList.setAdapter(mScheduleAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mScheduleAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mScheduleAdapter.startListening();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mRestauranteId = savedInstanceState.getString(Restaurante.REST_ID);
        mUserUID = savedInstanceState.getString(Usuario.USR_ID);
        mType = savedInstanceState.getInt(Usuario.USR_TYPE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_profile);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mRestauranteId = extras.getString(Restaurante.REST_ID);
            mUserUID = extras.getString(Usuario.USR_ID);
            mType = extras.getInt(Usuario.USR_TYPE);
        }
        initFirebase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mType == 0) {
            return true;
        }
        getMenuInflater().inflate(R.menu.fragment_rest_menu, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.create_dish_button: {
                Intent intent = new Intent(this, DishCreateActivity.class);
                intent.putExtra(Restaurante.REST_ID, mRestauranteId);
                startActivity(intent);
                break;
            }
            case R.id.create_schedule_button: {
                Intent intent = new Intent(this, ScheduleCreateActivity.class);
                intent.putExtra(Restaurante.REST_ID, mRestauranteId);
                startActivity(intent);
                break;
            }
            case R.id.edit_rest_button: {
                Intent intent = new Intent(this, RestEditActivity.class);
                intent.putExtra(Restaurante.REST_ID, mRestauranteId);
                startActivity(intent);
                break;
            }
        }
        return true;
    }
}