package so4id.gastrotec.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Usuario;

public class UserProfileActivity extends AppCompatActivity {

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.master_layout)
    LinearLayout mMasterLayout;
    @BindView(R.id.view_user_name_button)
    Button mNameButton;
    @BindView(R.id.view_user_license_button)
    Button mIdButton;
    @BindView(R.id.view_user_career_button)
    Button mCareerButton;
    @BindView(R.id.view_user_email_button)
    Button mEmailButton;
    @BindView(R.id.edit_user_button)
    Button mEditButton;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String mUid;

    @OnClick(R.id.edit_user_button)
    void onClick(View v) {
        Intent intent = new Intent(this, UserEditActivity.class);
        intent.putExtra(Usuario.USR_ID, mUid);
        startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Usuario.USR_ID, mUid);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mUid = savedInstanceState.getString(Usuario.USR_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            final String name = user.getDisplayName();
            final String email = user.getEmail();
            mUid = user.getUid();
            mDatabase = FirebaseDatabase.getInstance()
                    .getReference(Usuario.USR_LABEL).child(mUid);
            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Usuario user = dataSnapshot.getValue(Usuario.class);
                    mIdButton.setText(user.getCarnet());
                    mCareerButton.setText(user.getCarrera());
                    mNameButton.setText(name);
                    mEmailButton.setText(email);
                    mMasterLayout.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
