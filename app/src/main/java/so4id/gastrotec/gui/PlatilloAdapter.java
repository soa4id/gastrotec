package so4id.gastrotec.gui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import java.util.ArrayList;
import java.util.ListIterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Platillo;
import so4id.gastrotec.model.VotoPlatillo;


public class PlatilloAdapter extends FirebaseRecyclerAdapter<Platillo, PlatilloAdapter.Viewholder> {

    public PlatilloAdapter(FirebaseRecyclerOptions<Platillo> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(Viewholder holder, int position, Platillo model) {
        final Platillo platillo = this.getItem(position);
        holder.mDishName.setText(platillo.getNombre());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schedule_row, parent, false);
        return new PlatilloAdapter.Viewholder(view);
    }

    static class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.dish_name)
        TextView mDishName;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}