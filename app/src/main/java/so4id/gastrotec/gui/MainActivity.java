package so4id.gastrotec.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import so4id.gastrotec.Constants;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Restaurante;
import so4id.gastrotec.model.Usuario;

import so4id.gastrotec.model.Voto;
import so4id.gastrotec.model.VotoPlatillo;

public class MainActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 123;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.recycler_view_restaurants)
    RecyclerView mRecyclerViewContent;

    @BindView(R.id.master_layout)
    LinearLayoutCompat mMasterLayout;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    @BindView(R.id.progress_restaurant_list)
    ProgressBar mProgressBarList;
    RestaurantAdapter mAdapter;
    private long mIdUsuario = 1;
    private int mType = 0;
    private int mMaxRestaurants = 5;
    private FirebaseAuth mAuth;
    private ActionBarDrawerToggle mToggle;
    private DrawerLayout mDrawer;
    private DatabaseReference mDatabase;
    private String mUserId;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Usuario.USR_ID, mUserId);
        outState.putInt(Usuario.USR_TYPE, mType);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mUserId = savedInstanceState.getString(Usuario.USR_ID);
        mType = savedInstanceState.getInt(Usuario.USR_TYPE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null && mIdUsuario == -1) {
            mUserId = extras.getString(Usuario.USR_ID);
            mType = extras.getInt(Usuario.USR_TYPE);
        }

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            loggedIn(user);
        } else {
            startActivityForResult(
                    AuthUI.getInstance().createSignInIntentBuilder()
                            .setTheme(R.style.AppTheme).build(),
                    RC_SIGN_IN);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        mRecyclerViewContent.setLayoutManager(layoutManager);
        buildAdapter();

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();
                switch (menuItem.getItemId()) {
                    case R.id.navigation_user_profile: {
                        Intent intent = new Intent(getBaseContext(), UserProfileActivity.class);
                        intent.putExtra(Usuario.USR_ID, mIdUsuario);
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_add_rest: {
                        Intent intent = new Intent(getBaseContext(), RestCreateActivity.class);
                        intent.putExtra(Usuario.USR_TYPE, mType);
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_logout: {
                        AuthUI.getInstance()
                                .signOut(MainActivity.this)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    public void onComplete(@NonNull Task<Void> task) {
                                        mMasterLayout.setVisibility(View.GONE);
                                        startActivityForResult(
                                                AuthUI.getInstance().createSignInIntentBuilder()
                                                        .setTheme(R.style.AppTheme).build(),
                                                RC_SIGN_IN);
                                    }
                                });
                        break;
                    }
                }
                return true;
            }
        });

    }


    void buildAdapter() {
        mProgressBarList.setVisibility(View.VISIBLE);
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        mDatabase = db.getReference();

        Query query = mDatabase
                .child(Constants.FIREBASE_DATABASE_CHILD_RESTAURANTS)
                .limitToLast(mMaxRestaurants)
                .orderByChild(Restaurante.REST_VOTES);

        query.keepSynced(true);

        FirebaseRecyclerOptions<Restaurante> options =
                new FirebaseRecyclerOptions.Builder<Restaurante>()
                        .setQuery(query, Restaurante.class)
                        .build();

        RestaurantAdapter tempAdapter = new RestaurantAdapter(options, mUserId);
        if (mRecyclerViewContent.getAdapter() == null) {
            mRecyclerViewContent.setAdapter(tempAdapter);
        } else {
            mAdapter.stopListening();
            mRecyclerViewContent.swapAdapter(tempAdapter, false);
            tempAdapter.startListening();
        }
        mAdapter = tempAdapter;

        mProgressBarList.setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }

    void loggedIn(FirebaseUser user) {
        mMasterLayout.setVisibility(View.VISIBLE);
        final String name = user.getDisplayName();
        final String email = user.getEmail();
        final String uid = user.getUid();
        mUserId = uid;
        View header = mNavigationView.getHeaderView(0);
        TextView userNameTag = header.findViewById(R.id.nav_user_name);
        TextView userEmailTag = header.findViewById(R.id.nav_user_email);
        userNameTag.setText(name);
        userEmailTag.setText(email);
        mDatabase = FirebaseDatabase.getInstance().getReference(Usuario.USR_LABEL).child(uid);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String userType = (String) dataSnapshot.child(Usuario.USR_TYPE).getValue();
                if (userType == null) {
                    Intent intent = new Intent(getBaseContext(), RegisterActivity.class);
                    intent.putExtra(Usuario.USR_ID, uid);
                    startActivity(intent);
                } else {
                    if (userType.equals(Usuario.USR_ADMIN)) {
                        mType = 1;
                        mNavigationView.getMenu().findItem(R.id.navigation_add_rest).setVisible(true);
                        mAdapter.setType(mType);
                    } else {
                        mNavigationView.getMenu().findItem(R.id.navigation_add_rest).setVisible(false);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home: {
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START, false);
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == Activity.RESULT_OK) {
                loggedIn(mAuth.getCurrentUser());
            } else {
                if (response == null) {
                    finish();
                    return;
                }
                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    finish();
                    return;
                }
                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    finish();
                    return;
                }
            }

        }
    }

    @OnClick(R.id.btn_load_items)
    void loadMoreItems(View v) {
        mMaxRestaurants += 5;
        buildAdapter();
    }
}




