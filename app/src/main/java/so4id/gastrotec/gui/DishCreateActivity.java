package so4id.gastrotec.gui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Platillo;
import so4id.gastrotec.model.Restaurante;

public class DishCreateActivity extends AppCompatActivity {

    @BindView(R.id.dish_name_edit)
    EditText mDishNameEdit;
    private String mIdRestaurante;
    private DatabaseReference mDatabase;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Restaurante.REST_ID, mIdRestaurante);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mIdRestaurante = savedInstanceState.getString(Restaurante.REST_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_create);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mIdRestaurante = extras.getString(Restaurante.REST_ID);
        }
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @OnClick(R.id.create_dish_button)
    void onClick(View v) {
        String nombre = mDishNameEdit.getText().toString();
        if (TextUtils.isEmpty(nombre)) {
            Toast.makeText(getApplicationContext(), R.string.dish_no_name_provided,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Platillo plat = new Platillo();
        plat.setNombre(nombre);

        long platId = 0L;
        if (platId == -1) {
            Toast.makeText(getApplicationContext(), "Error al crear platillo",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Platillo registrado exitosamente",
                    Toast.LENGTH_SHORT).show();
            finish();
        }
        Platillo platillo = new Platillo(mIdRestaurante, nombre, 0);
        mDatabase.child(Restaurante.REST_LABEL).child(mIdRestaurante)
                .child(Platillo.PLAT_LABEL).push().setValue(platillo)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), R.string.dish_create_failed,
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.dish_created_successfully,
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
    }
}
