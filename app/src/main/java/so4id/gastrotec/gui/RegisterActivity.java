package so4id.gastrotec.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import so4id.gastrotec.Constants;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Usuario;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.autocomplete_id)
    AutoCompleteTextView mIdAutocomplete;
    @BindView(R.id.career_spinner)
    Spinner mCareerSpinner;

    private String mUid;
    private DatabaseReference mDatabase;

    @OnClick(R.id.registerx_button)
    void onClickRegister(View v) {

        String carnet = mIdAutocomplete.getText().toString();

        if (TextUtils.isEmpty(carnet)) {
            Toast.makeText(getApplicationContext(), R.string.register_no_id_provided,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Usuario user = new Usuario(new Usuario.Builder(mUid, carnet, mCareerSpinner.getSelectedItem().toString()
                , Usuario.USR_NML));

        mDatabase.child(Usuario.USR_LABEL).child(user.getId()).setValue(user);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Usuario.USR_ID, mUid);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mUid = extras.getString(Usuario.USR_ID);
        }
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Constants.DATABASE_CAREER).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final List<String> careers = new ArrayList<String>();
                for (DataSnapshot careerSnapshot : dataSnapshot.getChildren()) {
                    String areaName = careerSnapshot.getValue(String.class);
                    careers.add(areaName);
                }
                ArrayAdapter<String> areasAdapter = new ArrayAdapter<String>(getApplicationContext(),
                        android.R.layout.simple_spinner_item, careers);
                areasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mCareerSpinner.setAdapter(areasAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
