package so4id.gastrotec.gui;

import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import so4id.gastrotec.Constants;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Restaurante;
import so4id.gastrotec.model.Usuario;
import so4id.gastrotec.model.Voto;

public class RestaurantAdapter extends
        FirebaseRecyclerAdapter<Restaurante, RestaurantAdapter.Viewholder> {

    final String mUserId;
    int mType;

    public RestaurantAdapter(FirebaseRecyclerOptions<Restaurante> options, String pUserId) {
        super(options);
        this.mUserId = pUserId;
    }


    public void setType(int type) {
        this.mType = type;
    }

    void checkLike(String pRestId, final Viewholder holder) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference ref = db.getReference();
        Query ref2 = ref.child(Constants.FIREBASE_DATABASE_CHILD_RESTAURANTS)
                .child(pRestId)
                .child(Constants.FIREBASE_DATABASE_CHILD_VOTES)
                .child(Voto.DB_ID_USUARIO)
                .equalTo(mUserId);
        ref2.keepSynced(true);
        ref2.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Voto voto = dataSnapshot.getValue(Voto.class);
                int num = voto.getEstadoVoto();
                if (num == Voto.VOTO.DISLIKE) {
                    holder.mTgbDislike.setEnabled(false);
                    holder.mTgbDislike.setChecked(true);
                }
                else if (num == Voto.VOTO.LIKE) {
                    holder.mTgbLike.setEnabled(false);
                    holder.mTgbLike.setChecked(true);
                }
                else
                {
                    holder.mTgbDislike.setChecked(false);
                    holder.mTgbLike.setChecked(false);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Voto voto = dataSnapshot.getValue(Voto.class);
                int num = voto.getEstadoVoto();
                if (num == Voto.VOTO.DISLIKE) {
                    holder.mTgbDislike.setEnabled(false);
                    holder.mTgbDislike.setChecked(true);
                }
                else if (num == Voto.VOTO.LIKE) {
                    holder.mTgbLike.setEnabled(false);
                    holder.mTgbLike.setChecked(true);
                }
                else
                {
                    holder.mTgbDislike.setChecked(false);
                    holder.mTgbLike.setChecked(false);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onBindViewHolder(final Viewholder holder, final int position, Restaurante model) {
        final Restaurante restaurante = this.getItem(position);
        holder.mTextViewTitle.setText(restaurante.getNombre());
        holder.mTextViewVotes.setText(Integer.toString(restaurante.getBalanceVotos()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), RestProfileActivity.class);
                intent.putExtra(Restaurante.REST_ID, getRef(position).getKey());
                intent.putExtra(Usuario.USR_ID, mUserId);
                intent.putExtra(Usuario.USR_TYPE, mType);
                view.getContext().startActivity(intent);
            }
        });
        checkLike(getRef(position).getKey(), holder);

        holder.mTgbLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.mTgbLike.isChecked()) {
                    holder.mTgbLike.setEnabled(false);
                    holder.mTgbDislike.setEnabled(true);
                    holder.mTgbDislike.setChecked(false);
                    FirebaseDatabase db = FirebaseDatabase.getInstance();
                    DatabaseReference ref = db
                            .getReference(Constants.FIREBASE_DATABASE_CHILD_RESTAURANTS)
                            .child(getRef(position).getKey())
                            .child(Constants.FIREBASE_DATABASE_CHILD_VOTES);
                    ref.setValue(new Voto(mUserId, Voto.VOTO.LIKE));
                }
            }
        });

        holder.mTgbDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.mTgbDislike.isChecked()) {
                    holder.mTgbLike.setEnabled(true);
                    holder.mTgbDislike.setEnabled(false);
                    holder.mTgbLike.setChecked(false);
                    FirebaseDatabase db = FirebaseDatabase.getInstance();
                    DatabaseReference ref = db
                            .getReference(Constants.FIREBASE_DATABASE_CHILD_RESTAURANTS)
                            .child(getRef(position).getKey())
                            .child(Constants.FIREBASE_DATABASE_CHILD_VOTES);
                    ref.setValue(new Voto(mUserId, Voto.VOTO.DISLIKE));
                }
            }
        });
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_row, parent, false);
        return new Viewholder(view);
    }

    static class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.restaurant_txv_title)
        AppCompatTextView mTextViewTitle;

        @BindView(R.id.restaurant_txv_votes)
        AppCompatTextView mTextViewVotes;

        @BindView(R.id.restaurant_btn_like)
        ToggleButton mTgbLike;

        @BindView(R.id.restaurant_btn_dislike)
        ToggleButton mTgbDislike;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
