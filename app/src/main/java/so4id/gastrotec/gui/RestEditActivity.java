package so4id.gastrotec.gui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Restaurante;

public class RestEditActivity extends AppCompatActivity {

    @BindView(R.id.rest_name_edit)
    EditText mNameEditText;
    @BindView(R.id.rest_location_edit)
    EditText mLocationEditText;
    @BindView(R.id.working_hours_edit)
    EditText mWorkingHoursEditText;

    private String mIdRestaurante;
    private Restaurante rest;
    private DatabaseReference mDatabase;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Restaurante.REST_ID, mIdRestaurante);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mIdRestaurante = savedInstanceState.getString(Restaurante.REST_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_edit);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mIdRestaurante = extras.getString(Restaurante.REST_ID);
        }
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Restaurante.REST_LABEL).child(mIdRestaurante)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        rest = dataSnapshot.getValue(Restaurante.class);
                        mNameEditText.setText(rest.getNombre());
                        mLocationEditText.setText(rest.getDireccion());
                        mWorkingHoursEditText.setText(rest.getHorarioServicio());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });


    }

    @OnClick(R.id.save_restaurant_button)
    void onClick(View v) {
        String nombre = mNameEditText.getText().toString();
        String direccion = mLocationEditText.getText().toString();
        String horario = mWorkingHoursEditText.getText().toString();

        Restaurante restaurante = new Restaurante(nombre, direccion, horario, 0);
        mDatabase.child(Restaurante.REST_LABEL).child(mIdRestaurante).setValue(restaurante)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), R.string.rest_edit_failed,
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.rest_edit_successfully,
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}