package so4id.gastrotec.gui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import so4id.gastrotec.Constants;
import so4id.gastrotec.R;
import so4id.gastrotec.model.Usuario;

public class UserEditActivity extends AppCompatActivity {

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.master_layout)
    ScrollView mMasterLayout;
    @BindView(R.id.name_textview)
    EditText mNameTextView;
    @BindView(R.id.career_spinner)
    Spinner mCareerSpinner;
    @BindView(R.id.password_textview)
    EditText mPasswordTextView;
    @BindView(R.id.password_confirm_textview)
    EditText mPasswordConfirmTextView;

    private DatabaseReference mDatabase;
    private String mUid;
    private Usuario mUsuario;
    private FirebaseUser mFirebaseUser;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mUid = savedInstanceState.getString(Usuario.USR_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mUid = extras.getString(Usuario.USR_ID);
        }
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(Constants.DATABASE_CAREER).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final List<String> careers = new ArrayList<String>();
                for (DataSnapshot careerSnapshot : dataSnapshot.getChildren()) {
                    String areaName = careerSnapshot.getValue(String.class);
                    careers.add(areaName);
                }
                ArrayAdapter<String> areasAdapter = new ArrayAdapter<String>(getApplicationContext(),
                        android.R.layout.simple_spinner_item, careers);
                areasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mCareerSpinner.setAdapter(areasAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = firebaseAuth.getCurrentUser();
        if (mFirebaseUser != null) {
            final String name = mFirebaseUser.getDisplayName();
            mUid = mFirebaseUser.getUid();
            mDatabase = FirebaseDatabase.getInstance()
                    .getReference(Usuario.USR_LABEL).child(mUid);
            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mUsuario = dataSnapshot.getValue(Usuario.class);
                    mNameTextView.setText(name);
                    mMasterLayout.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @OnClick(R.id.save_user_button)
    void onClick(View v) {
        String pass = mPasswordTextView.getText().toString();
        String passConfirm = mPasswordConfirmTextView.getText().toString();
        String carrera = mCareerSpinner.getSelectedItem().toString();
        String name = mNameTextView.getText().toString();

        if (!TextUtils.isEmpty(pass)) {
            if (!pass.equals(passConfirm)) {
                mPasswordConfirmTextView.setError(getResources().getString(R.string.password_not_equal));
                return;
            } else if (pass.length() < 8) {
                mPasswordTextView.setError(getResources().getString(R.string.password_too_short));
                return;
            } else {
                mFirebaseUser.updatePassword(pass)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), R.string.password_update_error,
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        }

        if (!mUsuario.getCarrera().equals(carrera)) {
            mDatabase.child(Usuario.USR_CARRERA).setValue(carrera)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), R.string.career_update_error,
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            ;
        }

        if (!mFirebaseUser.getDisplayName().equals(name)) {
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(name)
                    .build();

            mFirebaseUser.updateProfile(profileUpdates)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), R.string.name_update_error,
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
        finish();
    }

}